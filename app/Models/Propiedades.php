<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propiedades extends Model
{
    protected $table = "propiedades"; 
    protected $fillable = ["clave","descripcion","tipo_transaccion","tipo_inmueble","estado","municipio","colonia","calle","numero","lat","lon"];
}
