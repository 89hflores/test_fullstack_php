<x-app-layout :user="$user">  
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="home">Propiedades</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        @if(isset($user->name))
                            <li> <a class="btn-outline-dark" href="list">Mis propiedades</a></li>
                        @endif 
                    </ul> 
                    <form class="d-flex">   
                        @if(isset($user->name))
                            Bienvenido {{$user->name}}/<a href="logout" class="btn-outline-dark">Cerrar Sesion</a>
                        @else
                            <a class="btn btn-outline-dark" href="login">
                                login
                            </a>    
                        @endif 
                    </form>
                </div>
            </div>
        </nav>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Listado de propiedades</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="#" title="Create a project"> <i class="fas fa-plus-circle"></i> </a>
            </div>
        </div>
        <div class="col-lg-2" >
             
        </div>
        <div class="col-lg-8">
            <table class="table table-bordered table-responsive-lg">
                <tr>
                    <th>Clave</th>
                    <th>Descripción</th>
                    <th>Tipo transaccion</th>
                    <th>Tipo Inmueble</th>
                    <th>Direccion</th> 
                    <th >Action</th>
                </tr>
                @foreach ($propiedades as $propiedad)
                    <tr>
                        <td>{{ $propiedad->clave }}</td>
                        <td>{{ $propiedad->descripcion }}</td>
                        <td>{{ $propiedad->tipo_transaccion }}</td>
                        <td>{{ $propiedad->tipo_inmueble }}</td>
                        <td>{{ $propiedad->municipio }}-{{ $propiedad->estado }}</td> 
                        <td>
                            <form action="{{ route('Propiedades.destroy', $propiedad->id) }}" method="POST">

                                <a href="{{ route('Propiedades.show', $propiedad->id) }}" title="show">
                                    <i class="fas fa-eye text-success  fa-lg"></i>
                                </a>

                                <a href="{{ route('Propiedades.edit', $propiedad->id) }}">
                                    <i class="fas fa-edit  fa-lg"></i>

                                </a>

                                @csrf
                                @method('DELETE')

                                <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                    <i class="fas fa-trash fa-lg text-danger"></i>

                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>    
    
</x-app-layout> 